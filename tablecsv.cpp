#include "tablecsv.h"

TableCsv::TableCsv(QString dir): QDialog ()
{    
    erropen=false;
    errread=false;
    QMessageBox msgBox;
    msgBox.setTextFormat(Qt::RichText);

    if (!QDir(dir).exists())
    {
        msgBox.setText("Data directory does not exist. Please double check your input.");
        msgBox.exec();
        erropen=true;
        return;
    }


    QFile file(QString("%1/samples.txt").arg(dir));
    if(!file.open(QIODevice::ReadOnly)) {
        msgBox.setText(QString("'samples.txt' cannot be opened from data directory. "
                       "Please create one for BDGE to preprocess your data. More information "
                       "can be found at <a href='%1/index.html#pp101'>BDGE</a> web page.").arg(Url_website));
        msgBox.exec();
        erropen=true;
        return;
    }

    QTextStream in(&file);
    QStringList *loadCsv;
    // QTableWidget *myTable=new QTableWidget();   future implementation

    nsample = 1;
    // read header
    loadCsv = new QStringList(in.readLine().split("\t"));
    if (loadCsv->size() > 4 || loadCsv->size() < 3)
    {
        msgBox.setText(QString("The header in 'samples.txt' file was wrong. "
                       "Please see an example at <a href='%1/index.html#pp101'>BDGE</a> web page "
                       "and make sure the file is Tab-delimited.").arg(Url_website));
        msgBox.exec();
        errread=true;
        delete loadCsv;
        return;
    }
    libraryname.push_back(loadCsv->at(0));
    librarynamesl << loadCsv->at(0);
    librarylayout.push_back(loadCsv->at(1));
    fastq1.push_back(loadCsv->at(2));
    if (loadCsv->size() == 3) {
        fastq2.push_back(QString(""));
        qDebug() << nsample << loadCsv->at(0) << loadCsv->at(1) << loadCsv->at(2);
    } else {
        fastq2.push_back(loadCsv->at(3));
        qDebug() << nsample << loadCsv->at(0) << loadCsv->at(1) << loadCsv->at(2) << loadCsv->at(3);
    }
    nsample++;
    delete loadCsv;

    while(!in.atEnd())
    {
        loadCsv = new QStringList(in.readLine().split("\t"));
        // myTable->setRowCount(nsample);  future implementation
        if (loadCsv->at(1) == "SINGLE" && loadCsv->size() > 3)
        {
            if (loadCsv->at(3).trimmed() != "")
            {
                msgBox.setText(QString("Single-end data should not have a non-empty entry on fastq2 column in 'samples.txt' file. "
                               "Please see an example at <a href='%1/index.html#pp101'>BDGE</a> web page. ").arg(Url_website));
                msgBox.exec();
                errread=true;
                delete loadCsv;
                return;
            }
        }
        /* myTable->setColumnCount(loadCsv->size());  future implementation
         * for(int col = 0; col < loadCsv->size(); ++col)
         * {
         *     QTableWidgetItem *items= new QTableWidgetItem(loadCsv->at(col));
         *     myTable->setItem((nsample - 1), col, items);
         * }
         */
        if (loadCsv->at(0).split(" ").size() > 1)
        {
            msgBox.setText(QString("Something was wrong with 'samples.txt' file. "
                           "LibraryName should not contain spaces. "
                           "Please visit <a href='%1/index.html#pp101'>BDGE</a> web page "
                           "for more information.").arg(Url_website));
            msgBox.exec();
            errread=true;
            delete loadCsv;
            return;
        }
        libraryname.push_back(loadCsv->at(0));
        librarynamesl << loadCsv->at(0);
        librarylayout.push_back(loadCsv->at(1));
        fastq1.push_back(loadCsv->at(2));
        if (loadCsv->size() == 3) {
            fastq2.push_back(QString(""));
            qDebug() << nsample << loadCsv->at(0) << loadCsv->at(1) << loadCsv->at(2);
        } else {
            fastq2.push_back(loadCsv->at(3));
            qDebug() << nsample << loadCsv->at(0) << loadCsv->at(1) << loadCsv->at(2) << loadCsv->at(3);
        }

        nsample++;
        delete loadCsv;
    }
    nsample = nsample-2;
    librarynamesl.removeFirst();
    librarynamesl.removeDuplicates();

    if (librarynamesl.size() != nsample)
    {
        msgBox.setText(QString("Something was wrong with 'samples.txt' file. "
                       "LibraryName should be unique. "
                       "Please visit <a href='%1/index.html#pp101'>BDGE</a> web page "
                       "for more information.").arg(Url_website));
        msgBox.exec();
        errread=true;
    }
    qDebug() << "Finish reading samples.txt";

    // reserved for future implementation
//    myTable->resizeColumnsToContents();

//    QDialogButtonBox *buttonBox = new QDialogButtonBox( Qt::Horizontal );
//    QPushButton *okButton = new QPushButton( tr("&Ok") );
//    QPushButton *cancelButton = new QPushButton( tr("&Cancel") );
//    buttonBox->addButton( okButton, QDialogButtonBox::AcceptRole );
//    buttonBox->addButton( cancelButton, QDialogButtonBox::RejectRole );

//    layout = new QVBoxLayout;
//    layout->addWidget(myTable);

//    createMenus(); // define pMenuBar
//    layout->setMenuBar(pMenuBar);
//    setLayout(layout);
//    setWindowTitle("samples.txt");
//    if (IncludeStyleSheet) setStyleSheet("background-color:rgb(236,219,187)");
//    resize(800, 400);

}

void TableCsv::createMenus()
{
    copyAction = new QAction(tr("&Copy"), this);
    copyAction->setIcon(QIcon("://images/copy.png"));
    copyAction->setShortcut(QKeySequence::Copy);
    copyAction->setStatusTip(tr("Copy the current selection's contents "
                                "to the clipboard"));
    connect(copyAction, SIGNAL(triggered()), this, SLOT(copy()));
    cutAction = new QAction(tr("C&upy"), this);
    cutAction->setIcon(QIcon("://images/cut.png"));
    cutAction->setShortcut(QKeySequence::Cut);
    cutAction->setStatusTip(tr("Cut the current selection's contents "
                               "to the clipboard"));
    connect(cutAction, SIGNAL(triggered()), this, SLOT(cut()));
    pasteAction = new QAction(tr("&Paste"), this);
    pasteAction->setIcon(QIcon("://images/paste.png"));
    pasteAction->setShortcut(QKeySequence::Paste);
    pasteAction->setStatusTip(tr("Paste the clipboard's contents into "
                                 "the current selection"));
    connect(pasteAction, SIGNAL(triggered()), this, SLOT(paste()));
    saveAction = new QAction(tr("&Save"), this);
    saveAction->setIcon(QIcon("://images/save.png"));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(save()));
    closeAction = new QAction(tr("C&lose"), this);
    closeAction->setIcon(QIcon("://images/exit.png"));
    closeAction->setShortcut(tr("Ctrl+Q"));
    closeAction->setStatusTip(tr("Close the dialog"));
    connect(closeAction, SIGNAL(triggered()), this, SLOT(close()));

    pMenuBar = new QMenuBar(this);
    fileMenu = pMenuBar->addMenu(tr("&File"));
    fileMenu->addAction(saveAction);
    fileMenu->addAction(closeAction);
    editMenu = pMenuBar->addMenu(tr("&Edit"));
    editMenu->addAction(copyAction);
    editMenu->addAction(cutAction);
    editMenu->addAction(pasteAction);
}

QTableWidgetSelectionRange TableCsv::selectedRange() const
{
//QList<QTableWidgetSelectionRange> ranges = selectedRanges();
//    if (ranges.isEmpty())
//        return QTableWidgetSelectionRange();
//    return ranges.first();
}

QString TableCsv::formula(int row, int column) const
{
//    Cell *c = cell(row, column);
//    if (c) {
//        return c->formula();
//    } else {
//        return "";
//    }
}
void TableCsv::copy()
{
//    QTableWidgetSelectionRange range = selectedRange();
//    QString str;

//    for (int i = 0; i < range.rowCount(); ++i) {
//        if (i > 0)
//            str += "\n";
//        for (int j = 0; j < range.columnCount(); ++j) {
//            if (j > 0)
//                str += "\t";
//            str += formula(range.topRow() + i, range.leftColumn() + j);
//        }
//    }
//    QApplication::clipboard()->setText(str);
}

void TableCsv::cut()
{

}

void TableCsv::paste()
{

}

void TableCsv::save()
{

}

void TableCsv::close()
{

}
