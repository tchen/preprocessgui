#include "toolsopt.h"

ToolsOpt::ToolsOpt(): QDialog()
{
    inputTophatOpt = "--no-coverage-search";
    inputHtseqOpt = "-s no -a 10";

    this->setWindowTitle("Tools Options");

    QGridLayout *layout = new QGridLayout(this);
    layout->setColumnStretch(1, 1);

    QString myString = tr("<p><font color='blue'>This dialog is used to specify non-default options "
                       "used in Tophat and HTSeq-count programs and the shell scripts 'run_bdge.sh' "
                          "will be generated accordingly. Note that if multi-thread option "
                          "'-p' is not specified in Tophat, the default number of threads will be "
                          "the number of CPU cores minus one.</font></p>");
    QTextEdit *textEdit = new QTextEdit;
    textEdit->setReadOnly (true);
    textEdit->setHtml( myString );
    textEdit->viewport()->setAutoFillBackground(false); // white is default
    layout->addWidget( textEdit, 0, 0, 1, 2);

    QDialogButtonBox *buttonBox = new QDialogButtonBox( Qt::Horizontal );
    QPushButton *okButton = new QPushButton( tr("&OK") );
    QPushButton *cancelButton = new QPushButton( tr("&Cancel") );
    buttonBox->addButton( okButton, QDialogButtonBox::AcceptRole );
    buttonBox->addButton( cancelButton, QDialogButtonBox::RejectRole );

    tophatEdit = new QLineEdit;
    tophatEdit->setText(inputTophatOpt);
    htseqEdit = new QLineEdit;
    htseqEdit->setText(inputHtseqOpt);

    layout->addWidget( new QLabel( tr("Tophat:")), 1, 0);
    layout->addWidget( tophatEdit, 1, 1 );

    layout->addWidget( new QLabel( tr("HTSeq-count:")), 2, 0);
    layout->addWidget( htseqEdit, 2, 1 );

    layout->addWidget(buttonBox, 3, 0, 1, 2);

    connect( okButton, SIGNAL(clicked()), this, SLOT(okClicked()) );
    connect( cancelButton, SIGNAL(clicked()), this, SLOT(cancelClicked()) );
}

void ToolsOpt::okClicked()
{
    inputTophatOpt = tophatEdit->text();
    inputHtseqOpt = htseqEdit->text();

    accept();
}

void ToolsOpt::cancelClicked()
{
    tophatEdit->setText(inputTophatOpt);
    htseqEdit->setText(inputHtseqOpt);

    reject();
}

QSize ToolsOpt::sizeHint() const
{
    return QSize(620, 250);
}
