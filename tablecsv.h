#ifndef TABLECSV_H
#define TABLECSV_H

#include <QtCore>
#include <QtGui>
#include "common.h"

class TableCsv : public QDialog
{
Q_OBJECT
public:
    TableCsv(QString);
    QVector<QString> libraryname;
    QVector<QString> librarylayout;
    QVector<QString> fastq1;
    QVector<QString> fastq2;
    int nsample;
    QStringList librarynamesl; // var used to check uniqueness of libraryname
    bool erropen, errread;
    QTableWidgetSelectionRange selectedRange() const;
private:
    QVBoxLayout *layout;
    void createMenus();
    QMenuBar* pMenuBar;
    QAction *copyAction;
    QAction *cutAction;
    QAction *pasteAction;
    QAction *saveAction;
    QAction *closeAction;
    QMenu *fileMenu;
    QMenu *editMenu;
    QString formula(int row, int column) const;
private slots:
    void copy();
    void cut();
    void paste();
    void save();
    void close();
};

#endif // TABLECSV_H
