#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtGui>
#include "toolsmng.h"
#include "tablecsv.h"
#include "banner.h"
#include "trimmer.h"
#include "common.h"
#include "toolsopt.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();

private slots:
  void sourceButton_clicked();
  void annotButton_clicked();
  void outputButton_clicked();
  void launchfastqc();
  void launchtoolsmng();
  void launchigv();
  void launchCalc();
  void launchtrimmer();
  void runUic();
  void handleError( QProcess::ProcessError );
  void handleFinish( int, QProcess::ExitStatus );
  void handleReadStandardError();
  void handleReadStandardOutput();
  void handleStarted();
  void handleStateChange( QProcess::ProcessState );
  void tutorial();
  void about();
  void openRecentFile();

private:
  void createMenus();
  void createGui();
  int checkSoftware();
  int createScript();
  void outputButton_change();
  void sourceButton_internal(const QString &fileName);
  QAction *quitAction;
  QAction *tmAction;
  QAction *toAction;
  QAction *igvAction;
  QAction *fastqcAction;
  QAction *trimmerAction;
  QAction *tutorialAction;
  QAction *aboutAction;
  QMenu *fileMenu;
  QMenu *settingMenu;
  QMenu *toolsMenu;
  QMenu *helpMenu;
  QAction *separatorAction;

  void readSettings();
  void saveRecentFile(const QString &fileName, const QString &fileName2, const QString &fileName3);
  void updateRecentFileActions();
  QString strippedName(const QString &fullFileName);
  QAction *recentFileActions[MaxRecentFiles];
  QStringList recentFiles, recentAnnotFiles, recentOutputFiles;

  QProcess *process;
  QPushButton *runButton, *sourceButton, *annotButton, *outputButton, *cancelButton, *sampleButton;
  QTextEdit *textEdit ;
  QLineEdit *sourceDir, *annotDir, *outputDir;
  QProgressBar *pbarDL;
  Banner *bnr;
  TableCsv *tc;
  ToolsMng *toolsmng;
  ToolsOpt *toolsopt;
  Trimmer *trm;
  QString workDir, genomeDir, outDir; // correspond to sourceDir, annotDir and outputDir
  int NumCore;
  QString bowind;
  QString gf;
  QFileSystemWatcher * watcher;
  QLabel *startTimeLabel, * endTimeLabel;
};

#endif // MAINWINDOW_H
