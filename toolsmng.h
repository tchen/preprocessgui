#ifndef TOOLSMNG_H
#define TOOLSMNG_H

#include <QtCore>
#include <QtGui>
#include "common.h"

class ToolsMng : public QDialog
{
    Q_OBJECT
public:
    explicit ToolsMng();
    QLineEdit *bowtieEdit, *tophatEdit, *samtoolsEdit, *htseqEdit, *sratoolkitEdit, *fastqcEdit, *trimmerEdit;
    QString inputBowtie, inputTophat, inputSamtools, inputHtseq, inputSratoolkit, inputFastQC, inputTrimmer;
    QSize sizeHint() const;
    void loadSettings();
    void saveSettings();
    QString m_sSettingsFile;
    // bool useAutosetup;
    void findPathSaveSettings();
private:
    QPushButton *brsBtBowtie, *brsBtTophat, *brsBtSamtools, *brsBtHtseq, *brsBtSratoolkit, *brsBtfastqc, *brsBttrimmer;
private slots:
    void cancelClicked();
    void okClicked();
    void autosetup();
    void brsBtTophat_clicked();
    void brsBtBowtie_clicked();
    void brsBtSamtools_clicked();
    void brsBtHtseq_clicked();
    void brsBtfastqc_clicked();
    void brsBttrimmer_clicked();
    void brsBtSratoolkit_clicked();
};

#endif // TOOLSMNG_H
