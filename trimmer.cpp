#include "trimmer.h"

Trimmer::Trimmer()
{
    workDir=QDir::homePath(); // browseButton_clicked() will re-define it
    TrimmerPath = QDir::homePath();  // MainWindow::launchtrimmer() will re-define it

    // UI
    runButton = new QPushButton(tr("&Execute trimming"));
    firstBase = new QLineEdit("1");
    lastBase = new QLineEdit("");
    lastBase->setToolTip(tr("Default is the entire read"));
    fastqFile = new QLineEdit("");
    browseButton = new QPushButton(tr("&Browse"));
    helpButton = new QPushButton(tr("&Help"));
    quitButton = new QPushButton(tr("&Quit"));
    outputFile = new QLineEdit("");
    outputFile->setToolTip(tr("For example, SRR001_trimmer.fastq"));

    pbarDL = new QProgressBar();
    pbarDL->setMaximum(10);
    pbarDL->setMinimum(0);
    pbarDL->setTextVisible(false);

    QString myString = tr("<p><font color='blue'>FastX_trimmer can be used to shorten reads in a FASTQ file (removing barcodes or noise).</font></p>");
    QTextEdit *textEdit = new QTextEdit;
    textEdit->setReadOnly (true);
    textEdit->setHtml( myString );
    textEdit->viewport()->setAutoFillBackground(false);

    QGridLayout *editLayout = new QGridLayout;
    editLayout->addWidget(textEdit, 0, 0, 1, 3);
    editLayout->addWidget(new QLabel(tr("Fastq file:")), 1, 0);
    editLayout->addWidget(fastqFile, 1, 1);
    editLayout->addWidget(browseButton, 1, 2);
    editLayout->addWidget(new QLabel(tr("First base to keep: ")), 2, 0);
    editLayout->addWidget(firstBase, 2, 1);
    editLayout->addWidget(new QLabel(tr("Last base to keep: ")), 3, 0);
    editLayout->addWidget(lastBase, 3, 1);
    editLayout->addWidget(new QLabel(tr("Output filename:")), 4, 0);
    editLayout->addWidget(outputFile, 4, 1);
    editLayout->addWidget(runButton, 5, 0, 1, 3);
    editLayout->addWidget(pbarDL, 6, 0, 1, 3);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *quitLayout = new QHBoxLayout;
    mainLayout->addLayout(editLayout);
    mainLayout->addStretch();
    mainLayout->addLayout(quitLayout);
    quitLayout->addStretch();
    quitLayout->addWidget(quitButton);
    quitLayout->addWidget(helpButton);

    setLayout(mainLayout);
    setWindowTitle(tr("FastX trimmer "));
    if (IncludeStyleSheet) setStyleSheet("background-color:rgb(236,219,187)");

    connect(browseButton, SIGNAL(clicked()), this, SLOT(browseButton_clicked()));
    connect(quitButton, SIGNAL(clicked()), this, SLOT(cancelclicked()));
    connect(runButton, SIGNAL(clicked()), this, SLOT(runTrimmer()));
}

void Trimmer::runTrimmer()
{    
    QString inFile = workDir + "/" + fastqFile->text();
    QString outFile = workDir + "/" + outputFile->text();
    QString fstBase = firstBase->text();
    QString lstBase = lastBase->text();
    QString command = QString("%1/bin/fastx_trimmer").arg(TrimmerPath);
    if (!fstBase.isEmpty()) {
        command = command + QString(" -f %1").arg(fstBase);
    }
    if (!lstBase.isEmpty()) {
        command = command + QString(" -l %1").arg(lstBase);
    }
    if (fastqFile->text().isEmpty()) {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("Input File name should not be empty"));
        return;
    }
    if (outputFile->text().isEmpty()) {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("Output File name should not be empty"));
        return;
    }
    command = command + QString(" -i \"%1\" -o \"%2\" -Q33").arg(inFile).arg(outFile);
    pbarDL->setMaximum(0);
    system(command.toStdString().c_str());

    pbarDL->setMaximum(10);
    pbarDL->setValue(10);
}

void Trimmer::cancelclicked()
{
    close();
}

void Trimmer::browseButton_clicked()
{
    QString initialName = fastqFile->text();
    if (initialName.isEmpty())
        initialName = workDir;
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Choose File"),
                                         workDir);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    // remove path from showing on the textEdit
    QFileInfo fi(fileName);
    QString name = fi.fileName();
    fastqFile->setText(name);
    workDir = fi.absolutePath();
}

QSize Trimmer::sizeHint() const
{
    return QSize(600, 200);
}
