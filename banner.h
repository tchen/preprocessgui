#ifndef BANNER_H
#define BANNER_H

#include <QtCore>
#include <QtGui>
#include "common.h"

class Banner : public QWidget
{
    Q_OBJECT
public:
    explicit Banner(QWidget *parent = 0);
    QSize sizeHint() const;
    QSize minimumSizeHint() const;
    int stage;
    QString msg;

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // BANNER_H
