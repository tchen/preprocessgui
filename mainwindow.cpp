#include "mainwindow.h"

MainWindow::MainWindow()
    : QMainWindow()
{
    // initialize variables
    NumCore=QThread::idealThreadCount() - 1;
    if (NumCore == 0) NumCore = 1;
    workDir = "";

    // begin menu setup
    createMenus();

    // begin Gui creation
    createGui();

    // Recent files.
    readSettings();

    // initialize variables
    process = 0;

    // setting/preference
    toolsmng = new ToolsMng;
    toolsmng->loadSettings();
    connect(tmAction, SIGNAL(triggered()), this, SLOT(launchtoolsmng())); // we like to check and fill out paths.

    toolsopt = new ToolsOpt;
    connect(toAction, SIGNAL(triggered()), toolsopt, SLOT(exec()));

    // connect for 3 pushbuttons.
    connect(sourceButton, SIGNAL(clicked()), this, SLOT(sourceButton_clicked()));
    connect(annotButton, SIGNAL(clicked()), this, SLOT(annotButton_clicked()));
    connect(outputButton, SIGNAL(clicked()), this, SLOT(outputButton_clicked()));

    // quit
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit())); // qApp is defined in header <QApplication>

    // launch fastqc    
    connect(fastqcAction, SIGNAL(triggered()), this, SLOT(launchfastqc()));

    // launch igv
    connect(igvAction, SIGNAL(triggered()), this, SLOT(launchigv()));

    connect(tutorialAction, SIGNAL(triggered()), this, SLOT(tutorial()));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));

    // launch fastx_trimmer
    trm = new Trimmer;
    connect(trimmerAction, SIGNAL(triggered()), this, SLOT(launchtrimmer()));

    connect(sampleButton, SIGNAL(clicked()), this, SLOT(launchCalc()));
    connect(runButton,    SIGNAL(clicked()), this, SLOT(runUic()) );

}

MainWindow::~MainWindow()
{

}

void MainWindow::createMenus()
{
    quitAction = new QAction("&Quit", this);
    tmAction = new QAction("&Tools Manager", this);
    toAction = new QAction("&Tools Options", this);
    igvAction = new QAction("&IGV", this);
    fastqcAction = new QAction("&FastQC", this);
    trimmerAction = new QAction("Fast&X_Trimmer", this);
    tutorialAction = new QAction("&Tutorial", this);
    aboutAction = new QAction("&About", this);

    for (int i = 0; i < MaxRecentFiles; ++i) {
        recentFileActions[i] = new QAction(this);
        recentFileActions[i]->setVisible(false);
        connect(recentFileActions[i], SIGNAL(triggered()),
                this, SLOT(openRecentFile()));
    }

    fileMenu = menuBar()->addMenu("&Data");
    separatorAction = fileMenu->addSeparator();
    for (int i = 0; i < MaxRecentFiles; ++i)
        fileMenu->addAction(recentFileActions[i]);
    fileMenu->addSeparator();
    fileMenu->addAction(quitAction);    
    settingMenu = menuBar()->addMenu("&Setting");
    settingMenu->addAction(tmAction);
    settingMenu->addAction(toAction);
    toolsMenu = menuBar()->addMenu("&Tools");
    toolsMenu->addAction(fastqcAction);
    toolsMenu->addAction(trimmerAction);
//    toolsMenu->addAction(igvAction);
    helpMenu = menuBar()->addMenu("&Help");
    helpMenu->addAction(tutorialAction);
    helpMenu->addAction(aboutAction);
}

void MainWindow::createGui()
{
    runButton = new QPushButton(tr("&Run Preprocessing"));
    textEdit = new QTextEdit;
    textEdit->setReadOnly(true);
    textEdit->setStyleSheet("background-image: url(://images/Pre-mRNA-1ysv-tubes.png)");
    sourceDir = new QLineEdit;
    sourceButton = new QPushButton(tr("&Browse"));
    annotDir = new QLineEdit;
    annotButton = new QPushButton(tr("Browse"));
    outputDir = new QLineEdit;
    outputButton = new QPushButton(tr("Browse"));
    sampleButton = new QPushButton(tr("&View/Edit samples.txt"));

    pbarDL = new QProgressBar();
    pbarDL->setMaximum(10);
    pbarDL->setMinimum(0);
    pbarDL->setTextVisible(false);

    startTimeLabel = new QLabel();
    startTimeLabel->setText("Start Time:                                  " );
    endTimeLabel = new QLabel();
    endTimeLabel->setText("End Time:                                  " );

    bnr = new Banner();
    bnr->setStyleSheet("background-color:black;");

    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(new QLabel("Data Dir:"), 1, 0);
    mainLayout->addWidget(sourceDir, 1, 1);
    mainLayout->addWidget(sourceButton, 1, 2);

    mainLayout->addWidget(new QLabel("Annotation Dir:"), 2, 0);
    mainLayout->addWidget(annotDir, 2, 1);
    mainLayout->addWidget(annotButton, 2, 2);

    mainLayout->addWidget(new QLabel("Output Dir:"), 3, 0);
    mainLayout->addWidget(outputDir, 3, 1);
    mainLayout->addWidget(outputButton, 3, 2);

    mainLayout->addWidget(sampleButton, 4, 0, 1, 3);

    mainLayout->addWidget(runButton, 5, 0, 1, 3);

    mainLayout->addWidget(textEdit, 6, 0, 1, 3);

    mainLayout->addWidget(bnr, 7, 0, 1, 3);

    setCentralWidget(new QWidget);
    centralWidget()->setLayout(mainLayout);
    setWindowTitle(tr("BRB Digital Gene Expression "));

    if (IncludeStyleSheet) setStyleSheet("background-color:rgb(236,219,187)");

    QStatusBar *statusBar = this->statusBar();
    statusBar->addPermanentWidget(pbarDL);
    statusBar->addWidget( startTimeLabel);
    statusBar->addWidget( endTimeLabel );

}

void MainWindow::sourceButton_clicked()
{
    QString initialName = sourceDir->text();
    if (initialName.isEmpty())
        initialName = QDir::homePath();
    QString fileName =
            QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }

    sourceButton_internal(fileName);
}



void MainWindow::sourceButton_internal(const QString &fileName)
{
    /* This is an internal function used by
     *   sourceButton_clicked()
     * and
     *   openRecentFile()
     */

    // set sourceDir
    sourceDir->setText(fileName);
    // set annotDir and outputDir
    int indexInRecentFiles = -1;
    for(int i=0; i<recentFiles.size(); i++)
        if (recentFiles.at(i).compare(fileName) == 0) {
            indexInRecentFiles = i;
            break;
        }
    if (indexInRecentFiles >=0 && recentAnnotFiles.size() > indexInRecentFiles)
    {
        annotDir->setText(recentAnnotFiles.at(indexInRecentFiles));
    } else {
        annotDir->setText(fileName); // set the default annotDir as sourceDir.
    }
    if (indexInRecentFiles >=0 && recentOutputFiles.size() > indexInRecentFiles)
    {
        outputDir->setText(recentOutputFiles.at(indexInRecentFiles));
    } else {
        // sourceDir/output
        outputDir->setText(fileName + "/output");
    }

    // set trimmer initial working directory
    trm->workDir = fileName;

    // update status
    bnr->stage=1;
    bnr->update();
}


void MainWindow::annotButton_clicked()
{
    QString initialName = annotDir->text();
    if (initialName.isEmpty())
        initialName = QDir::homePath();
    QString fileName =
            QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }

    // set annotDir
    annotDir->setText(fileName);
}

void MainWindow::outputButton_clicked()
{
    QString initialName = outputDir->text();
    if (initialName.isEmpty())
        initialName = QDir::homePath();
    QString fileName =
            QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }

    // set outputDir
    outputDir->setText(fileName);
}

void MainWindow::outputButton_change()
{
    // Called by runUic()/when the output folder already existed.
    QString initialName = sourceDir->text();
    if (initialName.isEmpty())
        initialName = QDir::homePath();
    QString fileName =
            QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }

    // set outputDir
    outputDir->setText(fileName);
}


void MainWindow::launchtoolsmng()
{
    QString m_sSettingsFile = "/home/" + QString(getenv("USER")) + "/.BDGE.ini";
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    if (settings.value("firstTime").toString().compare("") == 0)
        settings.setValue("firstTime", "false");
    bnr->stage=0; // return to the msg of browsing the data dir
    bnr->update();

    toolsmng->findPathSaveSettings();
    toolsmng->exec(); // Shows the dialog as a modal dialog.        
}

void MainWindow::launchfastqc()
{
    system(QString("%1/fastqc &").arg(toolsmng->inputFastQC).toStdString().c_str());
}

void MainWindow::launchtrimmer()
{
    trm->TrimmerPath = toolsmng->inputTrimmer;
    trm->exec();
}

void MainWindow::launchigv()
{
    system("/opt/RNA-Seq/bin/IGV_2.3.32/igv.sh &");
}

void MainWindow::launchCalc()
{
    if (workDir.compare("") ==0)  workDir = sourceDir->text();
    QString mystr = "/usr/bin/libreoffice --calc \"" + workDir + "/samples.txt\"";
    system(mystr.toStdString().c_str());
}

int MainWindow::checkSoftware()
{
    // return true if everything is OK, false if some program is not available
    // http://www.cyberciti.biz/faq/unix-linux-shell-find-out-posixcommand-exists-or-not/
    // command -v command1 >/dev/null && echo "command1 Found" || echo "command1 Not Found"
    // system("command -v /opt/RNA-Seq/bin/tophat-2.0.11.Linux_x86_64/tophat > /tmp/check"); // quick check; needs to be improved by QProcess
    system(QString("command -v %1/tophat > /tmp/check").arg(toolsmng->inputTophat).toStdString().c_str());
    int size = 0;
    QFile myFile("/tmp/check");
    if (myFile.open(QIODevice::ReadOnly)){
        size = myFile.size();  //when file does open.
        myFile.close();
        system("rm /tmp/check");
    }
    if (size == 0) return 1;

    system(QString("command -v %1/bowtie2 > /tmp/check").arg(toolsmng->inputBowtie).toStdString().c_str());
    QFile myFile2("/tmp/check");
    if (myFile2.open(QIODevice::ReadOnly)){
        size = myFile2.size();  //when file does open.
        myFile2.close();
        system("rm /tmp/check");
    }
    if (size == 0) return 2;

    system(QString("command -v %1/samtools > /tmp/check").arg(toolsmng->inputSamtools).toStdString().c_str());
    QFile myFile3("/tmp/check");
    if (myFile3.open(QIODevice::ReadOnly)){
        size = myFile3.size();  //when file does open.
        myFile3.close();
        system("rm /tmp/check");
    }
    if (size == 0) return 3;

    system("python -c 'import HTSeq' 2> /tmp/check");
    QFile myFile4("/tmp/check");
    if (myFile4.open(QIODevice::ReadOnly)){
        size = myFile4.size();  //when file does open.
        myFile4.close();
        system("rm /tmp/check");
    }
    if (size > 0) return 4;

    return 0;
}

int MainWindow::createScript()
{
    QString filename;
    // Create run_bdge.sh
    filename = sourceDir->text() + "/run_bdge.sh";
    QFile myfile(filename);
    if(!myfile.open(QFile::WriteOnly |
        QFile::Text))
    {
        qDebug() << QString("Could not open file %1 for writing").arg(filename);
        return 1;
    }
    myfile.setPermissions(QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner | QFile::ReadGroup | QFile::WriteGroup | QFile::ExeGroup | QFile::ReadOther);
    QTextStream out(&myfile);
    out << "#!/bin/bash\n";
    if (!toolsmng->inputBowtie.isEmpty())
    {
        out << "export bdge_bowtie_PATH=" + toolsmng->inputBowtie + "\n";
    } else {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("Bowtie could not be found. Please click Settings -> Preferences to set it up."));
        return 1;
    }
    if (!toolsmng->inputTophat.isEmpty())
    {
        out << "export bdge_tophat_PATH=" + toolsmng->inputTophat + "\n";
    } else {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("Tophat could not be found. Please click Settings -> Preferences to set it up."));
        return 1;
    }

    if (!toolsmng->inputSamtools.isEmpty())
    {
        out << "export bdge_samtools_PATH=" + toolsmng->inputSamtools + ":" + toolsmng->inputSamtools + "/bcftools:" + toolsmng->inputSamtools + "/misc\n";
    } else {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("Samtools could not be found. Please click Settings -> Preferences to set it up."));
        return 1;
    }
    out << "export PATH=$bdge_bowtie_PATH:$bdge_samtools_PATH:$bdge_tophat_PATH:$PATH\n\n";
    QStringList strlist;
    strlist = toolsopt->inputTophatOpt.split(" ");
    if (strlist.indexOf("-p") < 0 && strlist.indexOf("--num-threads") < 0) {
        strlist << QString("-p %1").arg(NumCore);
    }

    // clean out .count files if they are present
    out << QString("if [ ! -d \"%1\" ]; then mkdir \"%1\"; fi\n").arg(outDir);
    out << QString("if ls \"%1\"/*.count &> /dev/null; then rm '%1'/*.count ; fi\n").arg(outDir);

    // create a tmp subdir to store bam and sam files from running samtools
    out << QString("if [ ! -d tmp ]; then mkdir tmp; fi\n\n");

    // delete log.txt file if it exists
    out << QString("if [ -f tmp/log.txt ]; then rm tmp/log.txt; fi\n");
    out << QString("touch tmp/log.txt\n\n");

    for(int i=1; i <= tc->nsample; i++)
    {
        out << QString("# %1\n").arg(tc->libraryname[i]);
        out << QString("echo %1 `date +'%Y-%m-%d %T'` >> tmp/log.txt\n").arg(tc->libraryname[i]);
        out << QString("echo %1\n").arg(tc->libraryname[i]);
        out << QString("tophat2 %1 -o \"%2\" \"%3\" %4 %5\n") \
                            .arg(strlist.join(" ")) \
                            .arg(tc->libraryname[i]) \
                            .arg(bowind).arg(tc->fastq1[i]) \
                            .arg(tc->fastq2[i]);
        out << QString("samtools sort -n \"%1/accepted_hits.bam\" \"tmp/%1_sn\"\n").arg(tc->libraryname[i]);
        out << QString("samtools view -o \"tmp/%1_sn.sam\" \"tmp/%1_sn.bam\"\n").arg(tc->libraryname[i]);

        out << QString("python -m HTSeq.scripts.count %1 \"tmp/%2_sn.sam\" \"%3\" > \"%4/%2.count\"\n\n") \
                 .arg(toolsopt->inputHtseqOpt).arg(tc->libraryname[i]).arg(gf).arg(outDir)  ;
        out << QString("echo\n");
    }
    // Record the end time in the log file
    out << QString("echo completed `date +'%Y-%m-%d %T'` >> tmp/log.txt\n");

    myfile.flush();
    myfile.close();
    // End of creating run_bdge.sh

    return 0;
}

void MainWindow::runUic()
{
    // Function called when user clicked 'run' button.

    // Obtain directories from user's input
    workDir = sourceDir->text();
    genomeDir = annotDir->text();
    outDir = outputDir->text();

    // 1. check existence and integrity of samples.txt file
    tc = new TableCsv(workDir);
    if (tc->erropen || tc->errread) {
        delete tc;
        return;
    }

    // 2. check if ouput folder is empty or not.
    if ( QDir(outDir).entryInfoList(QDir::NoDotAndDotDot|QDir::AllEntries).count() > 0) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "BRB Digital Gene Expression",
                                      "The output directory is not empty. Do you want to overwrite files in this directory?",
                                        QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No) {
            outputButton_change();
            delete tc;
            return;
        }
    }

    // 3. check existence of bowtie index files (*.bt2)
    // genomeDir cannot contain spaces
    if (genomeDir.split(" ").size() > 1)
    {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("The full path to the annotation directory should not contain spaces. "
                                    "Please correct it or select a different location."));
        delete tc;
        return;
    }
    QStringList nameFilter("*.1.bt2");
    QDir directory(genomeDir);
    QStringList files = directory.entryList(nameFilter);
    if (files.size() ==0) {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 QString("No bowtie2 reference genome files (*.bt2) were found. "
                                    "Please download them from <a href='%1'>Tophat</a> website and try again."
                                    " For further help, see <a href='%2/index.html#Tut'>BDGE Tutorial</a> page.").arg(Url_igenomes).arg(Url_website));
        delete tc;
        return;
    }
    bowind = genomeDir + "/" + QFileInfo(files.at(0)).baseName(); // bowind="genome";

    // 4. check existence of gene annotation file (*.gtf)
    QStringList nameFilter2("*.gtf");
    QDir directory2(genomeDir);
    files = directory2.entryList(nameFilter2);
    if (files.size() ==0) {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 QString("No gene annotation file (*.gtf) was found. "
                                    "Please download it from <a href='%1'>Tophat</a> website and try again."
                                    " For further help, see <a href='%2/index.html#Tut'>BDGE Tutorial</a> page.").arg(Url_igenomes).arg(Url_website));
        delete tc;
        return;
    }
    gf = genomeDir + "/" + files.at(0); // gf = "genes.gtf";

    // 5. check tophat, bowtie, samtools, htseq are available or not.
    // if (toolsmng->useAutosetup)
    // {
    toolsmng->findPathSaveSettings();
    // }
    switch( checkSoftware() )
    {
        case 1:
        QMessageBox::information(this, tr("BRB Digital Gene Expression"), tr("Tophat cannot be found! Please click Settings -> Tools Manager to set up the software."));
        delete tc;
        return;
        break;
        case 2:
        QMessageBox::information(this, tr("BRB Digital Gene Expression"), tr("Bowtie2 cannot be found! Please click Settings -> Tools Manager to set up the software."));
        delete tc;
        return;
        break;
        case 3:
        QMessageBox::information(this, tr("BRB Digital Gene Expression"), tr("samtools cannot be found! Please click Settings -> Tools Manager to set up the software."));
        delete tc;
        return;
        break;
        case 4:
        QMessageBox::information(this, tr("BRB Digital Gene Expression"), tr("HTSeq cannot be found! Please click Settings -> Tools Manager to set up the software."));
        delete tc;
        return;
        break;
    }

    // Add to recentFiles
    saveRecentFile(workDir, genomeDir, outDir);

    startTimeLabel->setText("Start Time: " + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm"));


    // Update banner
    bnr->stage=6;
    bnr->msg=QString("Working on %1").arg(tc->libraryname[1]);
    bnr->update();

    runButton->setEnabled( false );
    textEdit->setText( "" );

    if (workDir.compare("") ==0) {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"), tr("Data directory cannot be empty!"));
        return;
    }

    // Create an execution script
    if (createScript() > 0) return;

    if( process )
        delete process;
    process = new QProcess( this );

    connect( process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(handleError(QProcess::ProcessError)) );
    connect( process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(handleFinish(int,QProcess::ExitStatus)) );
    connect( process, SIGNAL(readyReadStandardError()), this, SLOT(handleReadStandardError()) );
    connect( process, SIGNAL(readyReadStandardOutput()), this, SLOT(handleReadStandardOutput()) );
    connect( process, SIGNAL(started()), this, SLOT(handleStarted()) );
    // connect( process, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(handleStateChange(QProcess::ProcessState)) );
    pbarDL->setMaximum(0);
    process->setWorkingDirectory(workDir);
    process->start("bash", QStringList() << workDir + "/run_bdge.sh");

}


void MainWindow::handleError( QProcess::ProcessError error )
{
    QString errorText;

    switch( error )
    {
        case QProcess::FailedToStart:
        errorText = "Failed to start";
        break;
        case QProcess::Crashed:
        errorText = "Crashed";
        break;
        case QProcess::Timedout:
        errorText = "Timed out";
        break;
        case QProcess::WriteError:
        errorText = "Write error";
        break;
        case QProcess::ReadError:
        errorText = "Read error";
        break;
        case QProcess::UnknownError:
        errorText = "Unknown error";
        break;
    }

    textEdit->append( QString( "<p><b><font color=red>%1</font></b><p>" ).arg( errorText ) );
}

void MainWindow::handleFinish( int code, QProcess::ExitStatus status )
{
    QString statusText;

    switch( status )
    {
        case QProcess::NormalExit:
        statusText = "Normal exit";
        break;
        case QProcess::CrashExit:
        statusText = "Crash exit";
        break;
    }

    textEdit->append( QString( "<p><b>%1</b><p>" ).arg( statusText ) );

    // update status
    bnr->stage=5;
    bnr->update();

    // Zip counts file into <counts.txt>
    system(QString("cd \"%1\"; zip counts.zip *.count").arg(outDir).toStdString().c_str());

    // move <run_bdge.sh> to the tmp folder
    system(QString("cd \"%1\"; mv run_bdge.sh tmp/").arg(workDir).toStdString().c_str());

    pbarDL->setMaximum(10);
    pbarDL->setValue(10);

    endTimeLabel->setText("End Time: " + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm"));
    QMessageBox::information(this, tr("BRB Digital Gene Expression"), tr("Preprocessing has finished! \n\nThe count data for all samples are saved in the zip file <counts.zip> of the output directory."));

}

void MainWindow::handleReadStandardError()
{
    QString errorText = process->readAllStandardError();
    textEdit->append( QString( "<font color=red>%1</font>" ).arg( errorText ) );
}

void MainWindow::handleReadStandardOutput()
{
    QString outputText = process->readAllStandardOutput();
    // Assumptions: None of tophat,samtools,htseq gives standard output as far as I know.
    //              tophat and htseq give standard error. samtools sometimes gives standard error.
    //              The only place we got standard output is from the echo command in our script.

    textEdit->insertPlainText( outputText );    
    outputText = outputText.left(outputText.size()-1); // remove linefeed

    if (tc->librarynamesl.contains(outputText))
    {
        // make sure the outputText is really coming from echo.
        bnr->stage = 6;
        bnr->msg = QString("Working on '%1' sample ...").arg(outputText);
        bnr->update();
    }
}

void MainWindow::handleStarted()
{
    textEdit->append( QString("<p><b>Started</b><p>" ) );
}

void MainWindow::handleStateChange( QProcess::ProcessState state )
{
    QString stateText;

    switch( state )
    {
        case QProcess::NotRunning:
        stateText = "Not running";

        runButton->setEnabled( true );
        break;
        case QProcess::Starting:
        stateText = "Starting";
        break;
        case QProcess::Running:
        stateText = "Running";
        break;
    }

    textEdit->append( QString( "<p>New status: <b>%1</b><p>" ).arg( stateText ) );
}

void MainWindow::tutorial()
{
   QMessageBox::about(this, tr("BRB Digital Gene Expression"),
            QString("The tutorial can be found on the official <a href='%1/#tutorial'>BDGE web site</a>.").arg(Url_website));
}

void MainWindow::about()
{
   QMessageBox msgBox;
   msgBox.setWindowTitle(tr("About BRB Digital Gene Expression")),
   msgBox.setText("<h1 align='center'>BDGE &nbsp;&nbsp;&nbsp;</h1>"
                  "<p align='center'>Version 0.1 (Dec 2014) &nbsp;&nbsp;<br><br>"
                  "<u>System Architect</u>: &nbsp;&nbsp;<br>"
                  "Dr. Richard Simon &nbsp;&nbsp; <br>"
                  "Chief, Biometric Research Branch<br>"
                  "National Cancer Institute.<br><br>"
                  "<u>Development Team</u>: &nbsp;&nbsp;<br>"
                  "Dr. Ming-Chung Li, Dr. Lori Long, <br>"
                  "Dr. Ting Chen, Qihao Qi, Dr. Qian Xie<br><br>"
                  "The website of the BDGE software is:<br>"
                  "<a href='http://linus.nci.nih.gov/bdge'>http://linus.nci.nih.gov/bdge</a><br>"
                  "</p>");
   msgBox.exec();
}

void MainWindow::openRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)  {
        sourceButton_internal(action->data().toString());
    }
}

void MainWindow::readSettings()
{
    // Called by MainWindow() contructor.

    QString m_sSettingsFile = "/home/" + QString(getenv("USER")) + "/.BDGE.ini";
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    recentFiles = settings.value("recentFiles").toStringList();
    recentAnnotFiles = settings.value("recentAnnotFiles").toStringList();
    recentOutputFiles = settings.value("recentOutputFiles").toStringList();
    updateRecentFileActions();

    // for first time users, we instruct them to run auto setup
    if (settings.value("firstTime").toString().compare("") == 0) {
        bnr->stage=2;
        bnr->update();
    }
}

void MainWindow::saveRecentFile(const QString &fileName, const QString &fileName2, const QString &fileName3)
{
    // Called by run_Uic().

    QString m_sSettingsFile = "/home/" + QString(getenv("USER")) + "/.BDGE.ini";
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);

    // find the index of fileName
    int ind = recentFiles.indexOf(fileName);
    // save fileName (workDir)
    recentFiles.removeAt(ind);
    recentFiles.prepend(fileName);
    updateRecentFileActions();
    for(int j=MaxRecentFiles; j < recentFiles.size(); j++)
         recentFiles.removeAt(j);
    settings.setValue("recentFiles", recentFiles);

    // save fileName2 (genomeDir)
    if (recentAnnotFiles.size() > ind) recentAnnotFiles.removeAt(ind);
    recentAnnotFiles.prepend(fileName2);
    for(int j=MaxRecentFiles; j < recentAnnotFiles.size(); j++)
        recentAnnotFiles.removeAt(j);
    settings.setValue("recentAnnotFiles", recentAnnotFiles);

    // save fileName3 (outDir)
    if (recentOutputFiles.size() > ind) recentOutputFiles.removeAt(ind);
    recentOutputFiles.prepend(fileName3);
    for(int j=MaxRecentFiles; j < recentOutputFiles.size(); j++)
        recentOutputFiles.removeAt(j);
    settings.setValue("recentOutputFiles", recentOutputFiles);

}

void MainWindow::updateRecentFileActions()
{
    // Called by readSettings() and saveRecentFile().

    // We use the version in http://www.informit.com/articles/article.aspx?p=1405225&seqNum=4
    // instead of http://qt-project.org/doc/qt-4.8/mainwindows-recentfiles.html
    QMutableStringListIterator i(recentFiles);
    while (i.hasNext()) {
        if (!QFile::exists(i.next()))
            i.remove();
    }

    for (int j = 0; j < MaxRecentFiles; ++j) {
        if (j < recentFiles.count()) {
            QString text = tr("&%1 %2")
                           .arg(j + 1)
                           .arg(strippedName(recentFiles[j]));
            recentFileActions[j]->setText(text);
            recentFileActions[j]->setData(recentFiles[j]);
            recentFileActions[j]->setVisible(true);
        } else {
            recentFileActions[j]->setVisible(false);
        }
    }
    separatorAction->setVisible(!recentFiles.isEmpty());
}

QString MainWindow::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}
