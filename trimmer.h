#ifndef TRIMMER_H
#define TRIMMER_H

#include <QtCore>
#include <QtGui>
#include "common.h"

class Trimmer : public QDialog
{
    Q_OBJECT

public:
    Trimmer();
    QString workDir;
    QSize sizeHint() const;
    QString TrimmerPath;

private slots:
  void browseButton_clicked();
  void cancelclicked();
  void runTrimmer();

private:
    QPushButton *runButton, *browseButton, *helpButton, *quitButton;
    QLineEdit *firstBase, *lastBase, *fastqFile, *outputFile;
    QLabel *firstBaseLabel, *lastBaseLabel;
    QProgressBar *pbarDL;
    QString filepath;
};

#endif // TRIMMER_H
