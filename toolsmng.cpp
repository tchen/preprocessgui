#include "toolsmng.h"

ToolsMng::ToolsMng(): QDialog()
{
    m_sSettingsFile = "/home/" + QString(getenv("USER")) + "/.BDGE.ini";
    // useAutosetup = false; // obsolete

    this->setWindowTitle("Tools Manager");
    QGridLayout *layout = new QGridLayout(this);
    layout->setColumnStretch(1, 1);

    QDialogButtonBox *buttonBox = new QDialogButtonBox( Qt::Horizontal );
    QPushButton *okButton = new QPushButton( tr("&OK") );
    QPushButton *cancelButton = new QPushButton( tr("&Cancel") );
    buttonBox->addButton( okButton, QDialogButtonBox::AcceptRole );
    buttonBox->addButton( cancelButton, QDialogButtonBox::RejectRole );

     bowtieEdit = new QLineEdit;
     tophatEdit = new QLineEdit;
     samtoolsEdit = new QLineEdit;
     htseqEdit = new QLineEdit;
     sratoolkitEdit = new QLineEdit;
     fastqcEdit = new QLineEdit;
     trimmerEdit = new QLineEdit;

     inputBowtie = bowtieEdit->text();
     inputTophat = tophatEdit->text();
     inputSamtools = samtoolsEdit->text();
     inputHtseq = htseqEdit->text();
     inputSratoolkit = sratoolkitEdit->text();
     inputFastQC = fastqcEdit->text();
     inputTrimmer = trimmerEdit->text();

     brsBtBowtie = new QPushButton(tr("&Browse"));
     brsBtTophat = new QPushButton(tr("&Browse"));
     brsBtSamtools = new QPushButton(tr("&Browse"));
     brsBtHtseq = new QPushButton(tr("&Browse"));
     brsBtfastqc = new QPushButton(tr("&Browse"));
     brsBttrimmer = new QPushButton(tr("&Browse"));
     brsBtSratoolkit = new QPushButton(tr("&Browse"));

     QPushButton *autosetupButton = new QPushButton( tr("&Automatic Setup") );

     QString myString = tr("<p><font color='blue'>This dialog is used to set up the locations of required software. "
                        "You can choose either 'Automatic Setup' method or fill in the location of "
                        "each individual software. Note HTSeq should be available in the global environment.</font></p>");
     QTextEdit *textEdit = new QTextEdit;
     textEdit->setReadOnly (true);
     textEdit->setHtml( myString );
     textEdit->viewport()->setAutoFillBackground(false); // white is default
     layout->addWidget( textEdit, 0, 0, 1, 3);

     layout->addWidget( autosetupButton, 1, 0, 1, 3);

     layout->addWidget( new QLabel( tr("Bowtie Dir:") ), 2, 0);
     layout->addWidget( bowtieEdit, 2, 1 );
     layout->addWidget( brsBtBowtie, 2, 2);

     layout->addWidget( new QLabel( tr("Tophat Dir:")), 3, 0);
     layout->addWidget( tophatEdit, 3, 1 );
     layout->addWidget( brsBtTophat, 3, 2);

     layout->addWidget( new QLabel( tr("SAMtools Dir:")), 4, 0);
     layout->addWidget( samtoolsEdit, 4, 1 );
     layout->addWidget( brsBtSamtools, 4, 2);

//     HTSeq should be available in the global environment
//     layout->addWidget( new QLabel( tr("HTSeq Dir:")), 5, 0);
//     layout->addWidget( htseqEdit, 5, 1 );
//     layout->addWidget( brsBtHtseq, 5, 2);

     layout->addWidget( new QLabel( tr("SRA toolkit Dir:")), 6, 0);
     layout->addWidget( sratoolkitEdit, 6, 1 );
     layout->addWidget( brsBtSratoolkit, 6, 2);

     layout->addWidget( new QLabel( tr("FastQC Dir:")), 7, 0);
     layout->addWidget( fastqcEdit, 7, 1);
     layout->addWidget( brsBtfastqc, 7, 2);

     layout->addWidget( new QLabel( tr("FastX_trimmer Dir:")), 8, 0);
     layout->addWidget( trimmerEdit, 8, 1);
     layout->addWidget( brsBttrimmer, 8, 2);

     layout->addWidget(buttonBox, 9, 1, 1, 2);
     // setLayout(layout); We don't need this line if we use 'this' in new QGridLayout.

     if (IncludeStyleSheet) setStyleSheet("background-color:rgb(236,219,187)");

     connect(bowtieEdit, SIGNAL(returnPressed()), this, SLOT(okClicked())); // press 'Return' key will trigger something.
     connect(tophatEdit, SIGNAL(returnPressed()), this, SLOT(okClicked()));
     connect( okButton, SIGNAL(clicked()), this, SLOT(okClicked()) );
     connect( cancelButton, SIGNAL(clicked()), this, SLOT(cancelClicked()) );
     connect( autosetupButton, SIGNAL(clicked()), this, SLOT(autosetup()) );

     connect(brsBtTophat, SIGNAL(clicked()), this, SLOT(brsBtTophat_clicked()));
     connect(brsBtBowtie, SIGNAL(clicked()), this, SLOT(brsBtBowtie_clicked()));
     connect(brsBtSamtools, SIGNAL(clicked()), this, SLOT(brsBtSamtools_clicked()));
//     connect(brsBtHtseq, SIGNAL(clicked()), this, SLOT(brsBtHtseq_clicked()));
     connect(brsBtfastqc, SIGNAL(clicked()), this, SLOT(brsBtfastqc_clicked()));
     connect(brsBttrimmer, SIGNAL(clicked()), this, SLOT(brsBttrimmer_clicked()));
     connect(brsBtSratoolkit, SIGNAL(clicked()), this, SLOT(brsBtSratoolkit_clicked()));     
}

void ToolsMng::brsBtTophat_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    tophatEdit->setText(fileName);
    // inputTophat = fileName;
}

void ToolsMng::brsBtBowtie_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    bowtieEdit->setText(fileName);
    // inputBowtie = fileName;
}

void ToolsMng::brsBtSamtools_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    samtoolsEdit->setText(fileName);
    // inputSamtools = fileName;
}

void ToolsMng::brsBtHtseq_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    htseqEdit->setText(fileName);
    // inputHtseq = fileName;
}

void ToolsMng::brsBtfastqc_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    fastqcEdit->setText(fileName);
    // inputFastQC = fileName;
}

void ToolsMng::brsBttrimmer_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    trimmerEdit->setText(fileName);
    // inputTrimmer = fileName;
}

void ToolsMng::brsBtSratoolkit_clicked()
{
    QString initialName = QDir::homePath();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                         initialName);
    fileName = QDir::toNativeSeparators(fileName);
    if (fileName.isEmpty()) {
        return;
    }
    sratoolkitEdit->setText(fileName);
    // inputSratoolkit = fileName;
}


void ToolsMng::autosetup()
{
    /* Still not find a good solution
     * 1. If I use only one script, I need to use gnome-terminal to make the running in the foreground in order
     *    to receive user's input. But I could not know when the process is finished. So I cannot know
     *    the full path of each installed software; these are needed in order to run the next step.
     *    A possible solution is to move the identication of software path to the main program (use a boolean variable
     *    to check if user has run auto setup. If they do, we will read the software path.
     * 2. If I split the script into two scripts; one for download/extract and the other for build software.
     *    I will be able to know the software path when I directly use system("") to download/extract.
     *    However the process is run in the background and it may takes a while. Users may be lost.
     *    This approach seems to be worse than the first approach.
     */
    // useAutosetup = true;

    QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                             tr("A terminal window will pop up to install software. Please enter sudo password in the terminal to continue. "
                                "Once the installation is finished you can continue the analysis."));

    // QString install_sh_url = "https://raw.githubusercontent.com/arraytools/bdge/master/install_rnaseq.sh";
    QString install_sh_url = "wget -nv " + Url_install_script + " -O /tmp/install_rnaseq.sh";
    // qDebug() << install_sh_url;

    system(install_sh_url.toStdString().c_str()); // run wget

    // check if the internet connection is ok?
    int size = 0;
    QFile myFile("/tmp/install_rnaseq.sh");
    if (myFile.open(QIODevice::ReadOnly)){
        size = myFile.size();  //when file does open.
        myFile.close();
    }
    if (size == 0) {
        QMessageBox::information(this, tr("BRB Digital Gene Expression"),
                                 tr("Internet connection cannot be established. Please check your network settings."));
        return;
    } else {
        system("chmod +x /tmp/install_rnaseq.sh");
        system("gnome-terminal -x sudo /tmp/install_rnaseq.sh");
    }

    QDialog::accept();
}

void ToolsMng::findPathSaveSettings()
{
    /* Read /opt/RNA-Seq/bin directory to find each required software path.
     * At the end, the paths are saved to the setting file.
     *
     * These paths are used to define inputBowtie, .... variables and also written to parameter file.
     *
     * An example of the directory is
     *   bowtie2-2.2.1  fastx        samtools-0.1.19              tophat-2.0.11.Linux_x86_64
     *   FastQC         HTSeq-0.6.1  sratoolkit.2.3.5-2-ubuntu64
     *
     * The function is used by MainWindow::launchtoolsmng() and MainWindow:runUic().
     */
    QString path = "/opt/RNA-Seq/bin";
    QDir dir(path);
    QRegExp rxbowtie("bowtie");
    QRegExp rxtophat("tophat");
    QRegExp rxsamtools("samtools");
    // QRegExp rxhtseq("HTSeq");
    QRegExp rxfastqc("FastQC");
    QRegExp rxfastx("fastx");
    QRegExp rxsratoolkit("sratoolkit");

    foreach(QFileInfo item, dir.entryInfoList() )
    {
        // if the same keyword was found again, the last one will be used.
        if (bowtieEdit->text() == "" && rxbowtie.indexIn(item.absoluteFilePath()) >= 0)
        {
            inputBowtie = item.absoluteFilePath();
            bowtieEdit->setText(inputBowtie);
        }
        if (tophatEdit->text() == "" && rxtophat.indexIn(item.absoluteFilePath()) >= 0)
        {
            inputTophat = item.absoluteFilePath();
            tophatEdit->setText(inputTophat);
        }
        if (samtoolsEdit->text() == "" && rxsamtools.indexIn(item.absoluteFilePath()) >= 0)
        {
            inputSamtools = item.absoluteFilePath();
            samtoolsEdit->setText(inputSamtools);
        }
        // if (rxhtseq.indexIn(item.absoluteFilePath()) >= 0) inputHtseq = item.absoluteFilePath();htseqEdit->setText(inputHtseq);
        if (fastqcEdit->text() == ""  && rxfastqc.indexIn(item.absoluteFilePath()) >= 0)
        {
            inputFastQC = item.absoluteFilePath();
            fastqcEdit->setText(inputFastQC);
        }
        if (trimmerEdit->text() == "" && rxfastx.indexIn(item.absoluteFilePath()) >= 0)
        {
            inputTrimmer = item.absoluteFilePath();
            trimmerEdit->setText(inputTrimmer);
        }
        if (sratoolkitEdit->text() == "" && rxsratoolkit.indexIn(item.absoluteFilePath()) >= 0)
        {
            inputSratoolkit = item.absoluteFilePath();
            sratoolkitEdit->setText(inputSratoolkit);
        }
    }

    // write the paths to the ini file
    saveSettings();
}

void ToolsMng::loadSettings()
{
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    inputBowtie = settings.value("bowtie", "").toString();
    inputTophat = settings.value("tophat", "").toString();
    inputSamtools = settings.value("samtools", "").toString();
    // inputHtseq = settings.value("htseq", "").toString();
    inputSratoolkit = settings.value("sratoolkit", "").toString();
    inputFastQC = settings.value("fastqc", "").toString();
    inputTrimmer = settings.value("trimmer", "").toString();

    bowtieEdit->setText(inputBowtie);
    tophatEdit->setText(inputTophat);
    samtoolsEdit->setText(inputSamtools);
//    if (!inputHtseq.isEmpty()) {
//        htseqEdit->setText(inputHtseq);
//    } else {
//        htseqEdit->setText("");
//    }
    if (!inputSratoolkit.isEmpty()) {
        sratoolkitEdit->setText(inputSratoolkit);
    } else {
        sratoolkitEdit->setText("");
    }
    fastqcEdit->setText(inputFastQC);
    if (!inputTrimmer.isEmpty()) {
        trimmerEdit->setText(inputTrimmer);
    } else {
        trimmerEdit->setText("");
    }
}

void ToolsMng::saveSettings()
{
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    settings.setValue("bowtie", inputBowtie);
    settings.setValue("tophat", inputTophat);
    settings.setValue("samtools", inputSamtools);
//    settings.setValue("htseq", inputHtseq);
    settings.setValue("sratoolkit", inputSratoolkit);
    settings.setValue("fastqc", inputFastQC);
    settings.setValue("trimmer", inputTrimmer);
    settings.setValue("version", curVersion);
}

void ToolsMng::okClicked()
{
    // should the following be included in if (!useAutosetup)?
    inputBowtie = bowtieEdit->text();
    inputTophat = tophatEdit->text();
    inputSamtools = samtoolsEdit->text();
//    inputHtseq = htseqEdit->text();
    inputSratoolkit = sratoolkitEdit->text();
    inputFastQC = fastqcEdit->text();
    inputTrimmer = trimmerEdit->text();

    saveSettings();

    qDebug() << inputBowtie;
    qDebug() << inputTophat;
    qDebug() << inputSamtools;
//    qDebug() << inputHtseq;
    qDebug() << inputSratoolkit;
    qDebug() << inputFastQC;
    qDebug() << inputTrimmer;

    accept();
}

void ToolsMng::cancelClicked()
{
    bowtieEdit->setText(inputBowtie);
    tophatEdit->setText(inputTophat);
    samtoolsEdit->setText(inputSamtools);
//    htseqEdit->setText(inputHtseq);
    sratoolkitEdit->setText(inputSratoolkit);
    fastqcEdit->setText(inputFastQC);
    trimmerEdit->setText(inputTrimmer);
    reject();
}

QSize ToolsMng::sizeHint() const
{
    return QSize(800, 420);
}
