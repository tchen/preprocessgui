#include "banner.h"

Banner::Banner(QWidget *parent) :
    QWidget(parent)
{
    stage = 0;
    msg = "";
}

void Banner::paintEvent(QPaintEvent * event )
{
    Q_UNUSED( event );
    QPainter painter(this);
    QRect rect = QRect(0, 0, width()-1, height()-1);

    painter.setBrush(QBrush(Qt::black)); // black color
    painter.setPen(QColor("#ffffff"));   // white font color

    painter.drawRect(rect);

    switch(stage)  {
    case 0:
        painter.drawText(rect, Qt::AlignCenter, "Please select a data directory to start");
        break;
    case 1:
        painter.drawText(rect, Qt::AlignCenter, "Click 'Tools' > 'FastQC' to assess quality control and"
                                                 "'FastX_Trimmer' to trim the sequence. Or\n"
                         "check sample information in 'samples.txt' before click 'Run Preprocessing' to run preprocessing");
        break;
    case 2:
        painter.drawText(rect, Qt::AlignCenter, "For first time users, click Settings -> Tools Manager to set up required software");
        break;
    case 3:
        // painter.drawText(rect, Qt::AlignCenter, "Step 2/3: samtools is running ...");
        break;
    case 4:
        // painter.drawText(rect, Qt::AlignCenter, "Step 3/3: HTSeq-count is running ...");
        break;
    case 5:
        painter.drawText(rect, Qt::AlignCenter, "Misson accomplished  :)");
        break;
    case 6:
        painter.drawText(rect, Qt::AlignCenter, msg);
        break;
    }
    painter.end();
}

QSize Banner::minimumSizeHint() const
{
    return QSize(BannerWidth, BannerHeight);
}

QSize Banner::sizeHint() const
{
    return QSize(BannerWidth, BannerHeight);
}
