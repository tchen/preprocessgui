#ifndef TOOLSOPT_H
#define TOOLSOPT_H

#include <QtGui>
class ToolsOpt : public QDialog
{
    Q_OBJECT
public:
    explicit ToolsOpt();
    QLineEdit *tophatEdit, *htseqEdit;
    QString inputTophatOpt, inputHtseqOpt;
    QSize sizeHint() const;
private:
private slots:
    void cancelClicked();
    void okClicked();
};

#endif // TOOLSOPT_H
