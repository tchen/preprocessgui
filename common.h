#ifndef PREPROCESSGUI_H
#define PREPROCESSGUI_H

#define IncludeStyleSheet false
#define BannerWidth 801
#define BannerHeight 80
#define mainHeight 750
#define MaxRecentFiles  5
#define curVersion 0.1
const QString Url_install_script = "https://raw.githubusercontent.com/arraytools/bdge/master/install_rnaseq.sh";
const QString Url_website = "http://arraytools.github.io/bdge"; // http://linux.nci.nih.gov/bdge
const QString Url_igenomes = "http://ccb.jhu.edu/software/tophat/igenomes.shtml";

#endif // PREPROCESSGUI_H
